/*
   Oficial: www.transpeciessociety.com
   Autor: www.fenixbinario.com
   Dispositivo: Sentido del tiempo
   HW: Arduino Nano, 12 LED, Bateria, StepUp Reloj: SDA pinA4  SCL pinA5 GND pingnd VCC pin5v
   Descripcion: Arduino Nano conectado a un RTC DS 1307 y a 12 Led que se encienden en funcion de la hora a la que se encuentra.
   Esquema Comportamiento: https://drive.google.com/file/d/1Oj1V5XTvReO_pkk28CYbA0e2PqcHfSzU/view?usp=sharingr
   Esquema Electrico: ???
 * ** 20/06/2018 Creado por @fenixbinario
 * ** 20/06/2018 Modificado por @fenixbinario
*/

#define tempoDebug

#include <Wire.h>
#include "RTClib.h"

//LED
const byte una = 1 , dos = 2 , tres = 3 , cuatro = 4 , cinco = 5 , seis = 6 , siete = 7 , ocho = 8 , nueve = 9 , diez = 10 , once = 11 , cerodoce = 12;
const byte horario [] = {cerodoce, una, dos, tres, cuatro, cinco, seis, siete, ocho, nueve, diez, once};

//DS1037
RTC_DS1307 rtc;
DateTime rotacionHora;
byte cambioEstado = 0;


//salidas Pines
void pinSalidas(void)
{
  for ( byte led = 0; led <= 11 ; led++)
  {
    pinMode(horario[led], OUTPUT);
  }
}
//Convertir 1-24 a 1-12
bool AmPmPar (byte hora)
{
  if ( hora % 2 == 0) //es numero par
  {
    return true;
  }
  else return false;
}
byte AmPmFiltro(byte ap) //Filtramos el uso horario para que no salga por encima del 23h o por debajo de 0h
{
  if (ap == 24)
  {
    ap = 0;
  }

  return ap;
}
void apagarLed(void)
{
  for (byte led = 0; led <= 11 ; led++)
  {
    digitalWrite(horario[led], LOW);
  }
}

void encenderLed(byte hora)
{
  //Es numero Par? AmPmPar(hora) 0,2,4,6,8,10,12,14,16,18,20,22;
  if ( AmPmPar(hora) ) //es numero par y enciendo un Led
  {
    digitalWrite(horario[hora / 2], HIGH);
    #ifdef tempoDebug
    Serial.print("Pin Salida Par: ");
    Serial.println(horario[hora / 2], DEC);
    #endif
  }
  else//el numero es impar 1,3,5,7,9,11,13,15,17,19,21,23 y enciende dos Led
  {
    digitalWrite( horario[(hora - 1) / 2], HIGH );
    digitalWrite( horario[ (AmPmFiltro(hora + 1)) / 2 ], HIGH );
    #ifdef tempoDebug
    Serial.print("Pin Salida Impar -1: ");
    Serial.println((hora - 1) / 2, DEC);
    Serial.print("Pin Salida Impar +1: ");
    Serial.println((AmPmFiltro(hora + 1)) / 2, DEC);
    #endif
  }
}
void rotacionBegin(void)
{
  rotacionHora = rtc.now();
  byte hora = rotacionHora.hour();
  byte minuto = rotacionHora.minute();
  cambioEstado = hora;
  apagarLed();
  encenderLed(hora);

  #ifdef tempoDebug
  Serial.print("Hora: ");
  Serial.print(hora, DEC);
  Serial.print(" : " );
  Serial.print(minuto,DEC);
  #endif
}

void rotacionSolar (void)
{
  rotacionHora = rtc.now();
  byte hora = rotacionHora.hour();
  if (hora != cambioEstado)
  {
    cambioEstado = hora;
    apagarLed();
    encenderLed(hora);
  }

}

void setup() {
  //Serial
  Serial.begin(115200);
  if (! rtc.begin()) {
    Serial.println("No Se Encuentra RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC NO Está Funcionando!");
    // Configura el RTC con la fecha & hora justo al momento de compliar este sketch
    //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // Configura Fecha & Hora personalizada, por ejemplo
    // Enero 21, 2014 - 03:00:00 am :
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  else{
    Serial.println("RTC Está Funcionando!");
  }


  //Salidas del 1 al 12
  pinSalidas();

  //Inicializar Salidas
  rotacionBegin();

}

void loop()
{
  //Refrescar Control de Horario
  rotacionSolar();
}
